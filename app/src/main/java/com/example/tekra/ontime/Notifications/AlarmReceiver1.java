package com.example.tekra.ontime.Notifications;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;

/*
 * AlarmReceiver для отложенного вызова уведомлений*/
public class AlarmReceiver1 extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int number = intent.getIntExtra("first", 1);

        Intent service = new Intent(context, PushService1.class);
        service.putExtra("number", number);

        context.startService(service);
        //Отправляем уведомление*/
    }

}
