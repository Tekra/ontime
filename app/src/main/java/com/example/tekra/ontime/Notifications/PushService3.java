package com.example.tekra.ontime.Notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;
import com.example.tekra.ontime.Activitys.SelectedWayActivity;
import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;

public class PushService3 extends IntentService {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    private CompletedWayDatabase db;
    private int number;
    private SharedPreferences settings;

    public PushService3() {
        super("");
    }

    public PushService3(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);
        number = intent.getIntExtra("number", 1);


        if (intent != null) {
            sendNotification(number);
        }
    }


    private void sendNotification(int i) {

//String channelId = getString(R.string.default_notification_channel_id);


        if (i == 3000) {
            SharedPreferences.Editor editor = settings.edit();
            editor = settings.edit();
            editor.putBoolean("ImageThirdNotif",true);
            editor.apply();
            Intent notificationIntent = new Intent(this, SelectedWayActivity.class);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            PendingIntent contentIntent = PendingIntent.getActivity(this,
                    112, notificationIntent,
                    PendingIntent.FLAG_CANCEL_CURRENT);
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle("Начало маршрута через 5 минут")
                            .setSound(defaultSoundUri)
                            .setVibrate(new long[]{0, 500, 0})
                            .setAutoCancel(true)
                            .setContentIntent(contentIntent)
                            .setPriority(NotificationManager.IMPORTANCE_HIGH);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(3,
                    notificationBuilder.build());
            stopSelf();
            return;
        }
    }


    @Override
    public void onDestroy() {

        super.onDestroy();

    }
}
