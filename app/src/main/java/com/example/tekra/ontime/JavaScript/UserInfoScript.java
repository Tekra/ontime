package com.example.tekra.ontime.JavaScript;

import imangazaliev.scripto.java.ScriptoFunctionCall;

// для записи объекта получаемого из карты
public interface UserInfoScript {

    ScriptoFunctionCall<Void> loadUserData();

}
