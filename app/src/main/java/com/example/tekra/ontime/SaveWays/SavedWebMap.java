package com.example.tekra.ontime.SaveWays;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.example.tekra.ontime.JavaScript.MyJavaScriptInterface;
import com.example.tekra.ontime.R;

// для записи объекта получаемого из карты
public class SavedWebMap extends AppCompatActivity {
    WebView savedWebView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_web_map);
        savedWebView = findViewById(R.id.saved_web_view);
        savedWebView.getSettings().setJavaScriptEnabled(true);
        savedWebView.addJavascriptInterface(new MyJavaScriptInterface(this), "Android");
        savedWebView.loadUrl("file:///android_asset/SavedWebMap");
    }
}
