package com.example.tekra.ontime.RecyclerView;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tekra.ontime.R;

import java.util.ArrayList;

public class WayAdapterHistory extends RecyclerView.Adapter<WayAdapterHistory.WayViewHolder> {
    protected WayAdapterHistory.OnItemClickListener mOnItemClickListener;
    private ArrayList<WayModelHistory> ways = new ArrayList<>();

    public void setOnItemClickListener(WayAdapterHistory.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public WayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
// описываем как создается NoteViewHolder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_history_way_item, parent, false);
        return new WayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WayViewHolder holder, int position) {
// описываем как заполняется NoteViewHolder

        WayModelHistory model = ways.get(position);
        holder.endPoint.setText(model.getPointEnd());
        holder.startPoint.setText(model.getPointStart());
        holder.dateStart.setText(model.getDateStart());
        holder.timeEnd.setText(model.getTimeEnd());
        holder.missTime.setText(model.getMissTime());
        holder.isGoodWay.setImageResource(model.getIsGoodWay());
    }

    @Override
    public int getItemCount() {
        return ways.size();
    }

    public void updateNotes(ArrayList<WayModelHistory> ways) {
        this.ways.clear();
        this.ways.addAll(ways);
        notifyDataSetChanged(); // обновляет все видимые элементы
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class WayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Один элемент списка
        TextView startPoint;
        TextView endPoint;
        TextView timeEnd;
        TextView dateStart;
        TextView missTime;
        View category;
        View polosa;
        ImageView isGoodWay;


        public WayViewHolder(View itemView) {
            super(itemView);
// findViewById
            isGoodWay = itemView.findViewById(R.id.is_good_way);
            missTime = itemView.findViewById(R.id.miss_time);
            startPoint = itemView.findViewById(R.id.rv_point_start);
            endPoint = itemView.findViewById(R.id.rv_point_end);
            timeEnd = itemView.findViewById(R.id.rv_time_end);
            dateStart = itemView.findViewById(R.id.rv_date_start);
            category = itemView.findViewById(R.id.rv_image);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mOnItemClickListener.onItemClick(view, getAdapterPosition());
        }
    }


}