package com.example.tekra.ontime.Activitys;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class CalendarActivity extends AppCompatActivity {
    public String time;
    public int position1;
    FutureWaysActivity ftv = new FutureWaysActivity();
    Calendar dateAndTime = Calendar.getInstance();
    FloatingActionButton floatingActionButton;
    Intent intentWay;
    Calendar calendar = Calendar.getInstance();
    Calendar calendarMain;
    DatePickerDialog datePickerDialog;
                TimePickerDialog timePickerDialog;
    ScrollView scrollView;
    TextView textView;
    Intent intent2;
    Spinner spinner;
    private boolean dateChandged;
    private boolean timeChanged;
    private EditText editDate;
    private EditText editTime;
    // установка обработчика выбора времени
    TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        public String smallMinute;

        @SuppressLint("SetTextI18n")
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateAndTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
            dateAndTime.set(Calendar.MINUTE, minute);
            // timeChanged - flag который пропускает
            // в следующее Activity
            timeChanged = true;
            //Пример: Если минута равна 5, то писывает как 05
            if (dateAndTime.get(Calendar.MINUTE) < 10) {
                smallMinute = "0" + dateAndTime.get(Calendar.MINUTE);
            } else smallMinute = String.valueOf(dateAndTime.get(Calendar.MINUTE));

            editTime.setText(dateAndTime.get(Calendar.HOUR_OF_DAY) + ":" + smallMinute);
        }
    };
    private ArrayAdapter<String>[] arrForSpinner;
    private long timeFull;
    private SpinDatabase db;
    private List<Spin> spinList;
    private ArrayList<String> data = new ArrayList<>();
    private String origin;
    private String destination;
    private double duration;
    private double distance;
    ImageButton edit;
    ImageButton create;
    private String text;
    private String title;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        db = Room.databaseBuilder(getApplicationContext(),
                SpinDatabase.class, "database1").build();
        getListSpin();// инициализация БД
        editTime = findViewById(R.id.edit_time); //Edid выполняющий функцию pickerTime
        editDate = findViewById(R.id.edit_date); //Edit выполняющий функцию pickerDate
        edit = findViewById(R.id.edit);
        create = findViewById(R.id.create);
        scrollView = findViewById(R.id.scrolled_view);
        textView = findViewById(R.id.text_view_spinner);
        spinner = findViewById(R.id.spinner);




        /*
        Получение данных через intent из WebMapsActivity
         */
        intentWay = getIntent();
        origin = intentWay.getStringExtra("origin");
        destination = intentWay.getStringExtra("destination");
        duration = intentWay.getDoubleExtra("duration", 10.0);
        distance = intentWay.getDoubleExtra("distance", 10.0);
   /*     dateChandged = intentWay.getBooleanExtra("date",false);
        timeChanged = intentWay.getBooleanExtra("time",false);*/
       /* if (intentWay.getStringExtra("dateStr") != nullj)
        editDate.setText(intentWay.getStringExtra("dateStr"));
        if (intentWay.getStringExtra("timeStr" ) != nullj)
        editTime.setText(intentWay.getStringExtra("timeStr"));*/
        intent2 = new Intent(getApplicationContext(), DataActivity.class);
        // создание ссылки на Activity для записи данных в БД


        intent2.putExtra("origin", origin);

        intent2.putExtra("destination", destination);

        intent2.putExtra("duration", duration);

        intent2.putExtra("distance", distance);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spinList.size()!=0) {
                    Intent intent3 = new Intent(getApplicationContext(), CreateNewSpinnerActivity.class);
                    intent3.putExtra("title", title);
                    intent3.putExtra("text", text);
                    intent3.putExtra("origin", origin);
                    intent3.putExtra("destination", destination);
                    intent3.putExtra("duration", duration);
                    intent3.putExtra("pos", position1);
                    intent3.putExtra("distance", distance);
                    intent3.putExtra("edit", true);
                    startActivity(intent3);
                }
                else {
                    Toast.makeText(getApplicationContext(),"Нет списка для редактирования",Toast.LENGTH_SHORT).show();
                }
            }
        });
        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(getApplicationContext(),CreateNewSpinnerActivity.class);
                intent3.putExtra("title",title);
                intent3.putExtra("pos",position1);
                intent3.putExtra("text",text);
                intent3.putExtra("origin", origin);
                intent3.putExtra("pos",position1);
                intent3.putExtra("destination", destination);
                intent3.putExtra("duration", duration);
                intent3.putExtra("distance", distance);
                intent3.putExtra("edit", false);
                startActivity(intent3);
            }
        });
        setAdapter();
        // Запись данных в следующий intent
        /*
         * TODO:
         * когда происходит клик на item sprinner
         * меняется ListView на другой массив String
         * */
   /*     AdapterView.OnItemSelectedListener itemSelectedListener
    = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // Получаем выбранный объект
                String item = (String) parent.getItemAtPosition(position);

                if (item.equals("Добавить новое")) {
                    Intent intentForNewSprinner = new Intent(getApplicationContext(),CreateNewSpinnerActivity.class);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };*/

        // присваиваем адаптер списку
        // Ставим на EditText onClick


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка с onClick, по которой происходит переход по intent
        floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
          * Если не установлена дата
           * */
                if (!dateChandged && timeChanged) {
                    Snackbar.make(editDate, "Установите дату", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    } else

                                     if (!timeChanged && dateChandged) {
                                         Snackbar.make(editDate, "Установите время", Snackbar.LENGTH_LONG)
                                                 .setAction("Action", null).show();
                                     } else /*
                                      * Если не установлено  ничего
                                      * */
                                         if (!dateChandged && !timeChanged) {
                                             Snackbar.make(editDate, "Выберите дату и время", Snackbar.LENGTH_LONG)
                                                     .setAction("Action", null).show();
                                         } else {
                                             //запись выбранных "час" и "минута"
                                             calendarMain = new GregorianCalendar(datePickerDialog.getDatePicker().getYear(),
                                                     datePickerDialog.getDatePicker().getMonth(),
                                                     datePickerDialog.getDatePicker().getDayOfMonth(),
                                                     dateAndTime.get(Calendar.HOUR_OF_DAY),
                                                     dateAndTime.get(Calendar.MINUTE));
                                             long fifteenMinute = 15 * 60 * 1000;
                                             if (calendarMain.getTimeInMillis() > System.currentTimeMillis() + duration * 1000 + fifteenMinute && distance>1000) {
                                                 intent2.putExtra("hoursWay", dateAndTime.get(Calendar.HOUR_OF_DAY));
                                                 intent2.putExtra("minuteWay", dateAndTime.get(Calendar.MINUTE));
                                                 intent2.putExtra("Time", calendarMain.getTimeInMillis());
                                                 intent2.putExtra("textSpinner",textView.getText().toString());

                                                 Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                                                 NotificationCompat.Builder notificationBuilder =
                                                         new NotificationCompat.Builder(getApplicationContext())
                                                                 .setSmallIcon(R.drawable.logo)
                                                                 .setContentTitle("Маршрут добавлен")
                                                                 .setSound(defaultSoundUri)
                                                                                        .setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0,
                                                                                                new Intent(getApplicationContext(), FutureWaysActivity.class), 0))
                                                                                        .setVibrate(new long[]{0, 500, 0})
                                                                                        .setAutoCancel(true);


                                                                        NotificationManager notificationManager =
                                                                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                                                                        notificationManager.notify(85,
                                                                                notificationBuilder.build());
                                                                        if ((origin == null) || (destination == null) || (distance == 0) || (duration == 0)) {
                                                                            Snackbar.make(editDate, "Не удалось обработать запрос", Snackbar.LENGTH_LONG)
                                                                                    .setAction("Action", null).show();
                                                                        } else {
                                                                            startActivity(intent2);
                                                                        }
                                                                    } else {
                                                                        Snackbar.make(editDate, "Невозможно выполнить маршрут для выбранного времени", Snackbar.LENGTH_LONG)
                                                                                .setAction("Action", null).show();
                                                                    }

                                                    }}
                                                }

        );

        // Оповещение пользователя о том, что нужно выбрать
        Snackbar.make(scrollView, "Выберите дату и время", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        // Ставим на EditText onClick
    }



@Override
protected void onResume() {
    super.onResume();
    getListSpin();
    final Calendar c = Calendar.getInstance();
    int mYear = c.get(Calendar.YEAR); // текущий год
    int mMonth = c.get(Calendar.MONTH); // текущий месяц
    int mDay = c.get(Calendar.DAY_OF_MONTH); // текущий день
    // создаем date picker dialog
    timePickerDialog = new TimePickerDialog(CalendarActivity.this, timeSetListener,
            dateAndTime.get(Calendar.HOUR_OF_DAY),
            dateAndTime.get(Calendar.MINUTE), true);
    datePickerDialog = new DatePickerDialog(CalendarActivity.this,
            new DatePickerDialog.OnDateSetListener() {

                @SuppressLint("SetTextI18n")
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    // Устанавливаем текст выбранный в DatePicker
                    editDate.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                    datePickerDialog.getDatePicker().setFirstDayOfWeek(2);
                    // dateChanged - flag который пропускает
                    // в следующее Activity
                    dateChandged = true;
                    //запись в intent "день", "месяц", "год"
                    intent2.putExtra("day", dayOfMonth);
                    intent2.putExtra("month", monthOfYear + 1);
                    intent2.putExtra("year", year);
                    intent2.putExtra("position", position1);

                }
            }, mYear, mMonth, mDay);


    editDate.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            datePickerDialog.show();
        }
    });
    editTime.setOnClickListener(new View.OnClickListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View v) {

            timePickerDialog.show();
            // Устанавливаем текст выбранный в TimePicker


        }
    });
    setAdapter();
}
    public Calendar getCalendar() {
        return calendar;
    }

    public void getListSpin() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                spinList = db.getSpinnerDao().getAllSpins();
            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setAdapter()
    {


            data.clear();

        if (spinList.size()!= 0) {
            for (int i = 0; i < spinList.size(); i++) {
                data.add(spinList.get(i).titleSpinner);
            }
        }
        final ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, final int position, long id) {
                textView.setText(spinList.get(position).textSpinner);

                position1 = position;
                text = spinList.get(position).textSpinner;
                title = spinList.get(position).titleSpinner;
            }



            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(),WebMapsActivity.class));
    }
}
