package com.example.tekra.ontime.Notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmReceiver4 extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int number = intent.getIntExtra("four", 1);

        Intent service = new Intent(context, PushService4.class);
        service.putExtra("number", number);

        context.startService(service);

    }

}