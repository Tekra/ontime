package com.example.tekra.ontime.JavaScript;

import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.example.tekra.ontime.Activitys.WebMapsActivity;
import com.example.tekra.ontime.SaveWays.DirectionsResult;

public class MyJavaScriptInterface {

    public static DirectionsResult result;
    public static String origin;
    public static String destination;
    public static double duration;
    public static double distance;
    public WebMapsActivity wb = new WebMapsActivity();
    Context mContext;

    /**
     * Instantiate the interface and set the context
     */

    public MyJavaScriptInterface(Context c) {
        mContext = c;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        MyJavaScriptInterface.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        MyJavaScriptInterface.destination = destination;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        MyJavaScriptInterface.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        MyJavaScriptInterface.distance = distance;
    }

    @JavascriptInterface
    public void showUncorrectedAnswer() {
        wb.runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                Toast.makeText(mContext, "Error Way", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Сохранение данных из WebView
    @JavascriptInterface
    public void saveCallback(final String origin, final String destination, final double duration, final double distance) {
        setOrigin(origin);
        setDestination(destination);
        setDistance(distance);
        setDuration(duration);
    }

    @JavascriptInterface
    public void showCorrectedAnswer() {
        wb.runOnUiThread(new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                Toast.makeText(mContext, "Normal", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @JavascriptInterface
    public void saveTextResponse(final DirectionsResult response) {
        result = response;
        wb.runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }


/*    @JavascriptInterface
    public void saveResponse(final DirectionsResult response) {


        wb.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                *//*for (int i = 0; i < response.rows.length; i++) {
                    for (int j = 0; j < response.rows[i].element.length; j++) {
                        Toast.makeText(getmContext(),response.rows[i].element[i].distance.text + "LOL",Toast.LENGTH_SHORT).show();
                    }
                }

            }*//*
            }
        });
    }

    @JavascriptInterface
    public DirectionsResult getResponse()
    {
        return result;
    }*/

    /**
     * Show a toast from the web page
     */


}