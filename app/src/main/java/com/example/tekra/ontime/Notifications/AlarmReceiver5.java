package com.example.tekra.ontime.Notifications;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;

public class AlarmReceiver5 extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int number = intent.getIntExtra("five", 1);

        Intent service = new Intent(context, PushService5.class);
        service.putExtra("number", number);

        context.startService(service);
        //Отправляем уведомление*/
    }

}
