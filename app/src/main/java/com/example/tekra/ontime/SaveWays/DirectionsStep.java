package com.example.tekra.ontime.SaveWays;

import android.graphics.Point;

import com.google.android.gms.maps.model.LatLng;

// для записи объекта получаемого из карты
class DirectionsStep {
    public String instructions;
    public Distance distance;
    public Duration duration;
    public LatLng start_location;
    public LatLng end_location;
    public Point polyline;
    public DirectionsStep[] steps;
    public TravelMode travel_mode;
    public LatLng[] path;
    public TransitDetails transit;
}
