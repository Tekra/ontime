package com.example.tekra.ontime.Activitys;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.HistoryWay;
import com.example.tekra.ontime.Database.HistoryWayDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.Notifications.AlarmReceiver1;
import com.example.tekra.ontime.Notifications.AlarmReceiver2;
import com.example.tekra.ontime.Notifications.AlarmReceiver3;
import com.example.tekra.ontime.Notifications.AlarmReceiver4;
import com.example.tekra.ontime.Notifications.AlarmReceiver5;
import com.example.tekra.ontime.R;

import static android.app.AlarmManager.RTC_WAKEUP;

public class SelectedWayActivity extends AppCompatActivity

        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    ImageView nullWayImage;
    ImageView startWay;
    ImageView endWay;
    ImageView createWay;
    ImageView waitWay;
    ImageView toDoWay;
    ImageView polosa;
    TextView startPoint;
    TextView time;
    TextView date;
    TextView endPoint;
    TextView distance;
    TextView timeToWay;
    CompletedWay selectedWay;
    Intent intent;
    Intent intentR;
    ImageButton info;
    ImageButton start;
    ImageButton finish;
    AlarmManager alarmManager1;
    AlarmManager alarmManager2;
    AlarmManager alarmManager3;
    AlarmManager alarmManager4;
    AlarmManager alarmManager5;
    PendingIntent pendingIntent1;
    PendingIntent pendingIntent2;
    PendingIntent pendingIntent3;
    PendingIntent pendingIntent4;
    PendingIntent pendingIntent5;
    private SharedPreferences settings;
    private Context context;
    private CompletedWayDatabase db;
    private HistoryWayDatabase dbHistory;
    private TextView textView8;
    private TextView textView9;
    private TextView textView10;
    private TextView textView11;
    private TextView textView12;
    private double wayTime;
    private TextView textView5;
    private TextView textView3;
    private SwipeRefreshLayout swipeRefreshLayout;
    private WayDatabase dbWay;
    private ImageView polosa2;
    private boolean startCheck;
    private boolean finishCheck;
    private boolean startButtonBool;
    private boolean finishButtonBool;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_way);
        intent = getIntent();
        boolean bool = intent.getBooleanExtra("bool", false);
       /* if (bool)
            if(selectedWay!=null)
                pushToWay(selectedWay);*/

        nullWayImage = findViewById(R.id.nullSelectedWayImage);
        nullWayImage.setVisibility(View.INVISIBLE);
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor;
        editor = settings.edit();
        editor.putBoolean("startButton",false);
        editor.putBoolean("finishButton",false);
        editor.apply();
        new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                setImagesStep();
            }
        };
     /*   if (settings.contains(ON_TIME_PREFERENCES)) {
            position = (settings.getInt(ON_TIME_PREFERENCES, -1));
        }*/
        info = findViewById(R.id.imageInfo);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SelectedWayActivity.this);

                builder.setTitle("Список вещей")
                        .setMessage(selectedWay.textSpinner)
                        .setIcon(R.drawable.white_info)
                        .setCancelable(true)
                        .setNeutralButton("Ок", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        db = Room.databaseBuilder(getApplicationContext(),
                CompletedWayDatabase.class, "database5").build(); //
        dbHistory = Room.databaseBuilder(getApplicationContext(),
                HistoryWayDatabase.class, "database4").build();
        dbWay = Room.databaseBuilder(getApplicationContext(),
                WayDatabase.class,"database15").build();
        setSelectedWay();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        context = getApplicationContext();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        textView8 = findViewById(R.id.textView8);
        textView9 = findViewById(R.id.textView9);
        textView10 = findViewById(R.id.textView10);
        textView11 = findViewById(R.id.textView11);
        textView12 = findViewById(R.id.textView12);
        textView3 = findViewById(R.id.textView3);
        textView5 = findViewById(R.id.textView5);
        startWay = findViewById(R.id.image_startWay);
        createWay = findViewById(R.id.image_createWay);
        endWay = findViewById(R.id.image_endWay);
        toDoWay = findViewById(R.id.image_startToDo);
        waitWay = findViewById(R.id.image_waitStart);
        startPoint = findViewById(R.id.startSelectedPoint);
        endPoint = findViewById(R.id.endSelectedPoint);
        time = findViewById(R.id.timeText);
        date = findViewById(R.id.dateText);
        timeToWay = findViewById(R.id.timeToWay);
        distance = findViewById(R.id.distance);
        polosa = findViewById(R.id.imageView4);
        polosa2 = findViewById(R.id.imageView5);
        start = findViewById(R.id.imageButtonStart);
        finish = findViewById(R.id.imageButtonFinish);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTimeStart(true);
                setVisibleButton();
            }
        });
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setTimeFinish(true);
                setVisibleButton();
            }
        });
        start.setVisibility(View.INVISIBLE);
        finish.setVisibility(View.INVISIBLE);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        if (selectedWay != null) {
            nullWayImage.setVisibility(View.INVISIBLE);

            if (bool) {
                SharedPreferences.Editor editor1 = settings.edit();
                editor1.putBoolean("ImageFirstNotif", false);
                editor1.putBoolean("ImageSecondNotif", false);
                editor1.putBoolean("ImageThirdNotif", false);
                editor1.putBoolean("ImageFourNotif", false);
                editor1.putBoolean("ImageFiveNotif", false);
                editor1.putBoolean("startButton",false);
                editor1.putBoolean("finishButton",false);
                editor1.apply();
                startAllAlarms();
            }
            startPoint.setText(selectedWay.startPoint);
            endPoint.setText(selectedWay.endPoint);
            time.setText(getZero(selectedWay.endHour) + selectedWay.endHour + ":" + getZero(selectedWay.endMinute) + selectedWay.endMinute);
            date.setText(selectedWay.dayStart + " " + getMonth(selectedWay.month));
            int hours = (int) (selectedWay.wayTime / 60 / 60);
            int minute = (int) (selectedWay.wayTime / 60 - hours * 60);
            timeToWay.setText(getZero(hours) + hours + ":" + getZero(minute) + minute);
            int distanceTrip = (int) (selectedWay.distance / 100);
            distance.setText(String.valueOf(distanceTrip / 10) + "km");
        } else {
            setAllInvisible();
            nullWayImage.setVisibility(View.VISIBLE);
        }
    }

    private String getZero(int i) {
        if (i < 10) {
            return "0";
        } else {
            return "";
        }
    }

    private void setAllInvisible() {
        textView8.setVisibility(View.INVISIBLE);
        textView9.setVisibility(View.INVISIBLE);
        textView10.setVisibility(View.INVISIBLE);
        textView11.setVisibility(View.INVISIBLE);
        textView12.setVisibility(View.INVISIBLE);
        textView3.setVisibility(View.INVISIBLE);
        textView5.setVisibility(View.INVISIBLE);
        startPoint.setVisibility(View.INVISIBLE);
        endWay.setVisibility(View.INVISIBLE);
        createWay.setVisibility(View.INVISIBLE);
        startWay.setVisibility(View.INVISIBLE);
        waitWay.setVisibility(View.INVISIBLE);
        toDoWay.setVisibility(View.INVISIBLE);
        time.setVisibility(View.INVISIBLE);
        date.setVisibility(View.INVISIBLE);
        endPoint.setVisibility(View.INVISIBLE);
        distance.setVisibility(View.INVISIBLE);
        timeToWay.setVisibility(View.INVISIBLE);
        polosa.setVisibility(View.INVISIBLE);
        polosa2.setVisibility(View.INVISIBLE);
        start.setVisibility(View.INVISIBLE);
        finish.setVisibility(View.INVISIBLE);
        info.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setVisibleButton();
        if (selectedWay != null)
            intentR = getIntent();
        if (intentR != null) {
        startCheck = intentR.getBooleanExtra("start", false);
        finishCheck = intentR.getBooleanExtra("end", false);

            if (startCheck) {
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putBoolean("ImageFourNotif", true);
                editor.putBoolean("startButton",false);
                editor.putLong("timeStartWay", System.currentTimeMillis());
                editor.apply();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.cancel(4);
                }

            }
            if (finishCheck) {
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putBoolean("ImageFiveNotif", true);
                editor.putBoolean("finishButton",false);
                editor.putLong("timeEndWay", System.currentTimeMillis());
                editor.apply();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                if (notificationManager != null) {
                    notificationManager.cancel(5);
                }

            }
        }
        setImagesStep();

    }

    private void setTimeFinish(boolean finishCheck) {

        if (finishCheck) {
            SharedPreferences.Editor editor;
            editor = settings.edit();
            editor.putBoolean("ImageFiveNotif", true);
            editor.putLong("timeEndWay", System.currentTimeMillis());
            editor.putBoolean("finishButton",false);
            editor.apply();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancel(5);
            }

        }
    }


    private void setTimeStart(boolean startCheck) {
        if (startCheck) {
            SharedPreferences.Editor editor;
            editor = settings.edit();
            editor.putBoolean("ImageFourNotif", true);
            editor.putBoolean("startButton",false);
            editor.putLong("timeStartWay", System.currentTimeMillis());
            editor.apply();
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancel(4);
            }
        }
    }
    public void setVisibleButton()
    {
        startButtonBool = settings.getBoolean("startButton",false);
        finishButtonBool = settings.getBoolean("finishButton",false);
        if(startButtonBool)
            start.setVisibility(View.VISIBLE);
        else
            start.setVisibility(View.INVISIBLE);
        if(finishButtonBool) {
            start.setVisibility(View.INVISIBLE);
            finish.setVisibility(View.VISIBLE);
        }
        else {
            finish.setVisibility(View.INVISIBLE);
        }
        setImagesStep();
    }

    private void setImagesStep() {
        Intent intentForTime = getIntent();
        createWay.setImageResource(R.drawable.kvadratwithcheck);


        if (settings.getBoolean("ImageSecondNotif", false)) {
            waitWay.setImageResource(R.drawable.kvadratwithcheck);
        } else {
            waitWay.setImageResource(R.drawable.kvadrat);
        }
        if (settings.getBoolean("ImageThirdNotif", false)) {
            toDoWay.setImageResource(R.drawable.kvadratwithcheck);
        } else {
            toDoWay.setImageResource(R.drawable.kvadrat);
        }
        if (settings.getBoolean("ImageFourNotif", false)) {
            startWay.setImageResource(R.drawable.kvadratwithcheck);
            start.setVisibility(View.INVISIBLE);
        } else {
            startWay.setImageResource(R.drawable.kvadrat);
        }
        if (settings.getBoolean("ImageFiveNotif", false)) {
            endWay.setImageResource(R.drawable.kvadratwithcheck);
            finish.setVisibility(View.INVISIBLE);
            if (!settings.getBoolean("ImageFourNotif", false)) {
                Toast.makeText(getApplicationContext(), "Начало маршрута не было отмечено."
                        + " Путь был удален", Toast.LENGTH_LONG).show();
                delete();
                startActivity(new Intent(getApplicationContext(), FutureWaysActivity.class));
            } else if (getMissTime(selectedWay) > 20 || getMissTime(selectedWay) < -20) {

                String message;
                AlertDialog.Builder builder = new AlertDialog.Builder(SelectedWayActivity.this);
                if (getMissTime(selectedWay) > 20) {
                    message = "Вы прибыли слишком рано для запланированного времени -> "
                            + selectedWay.endHour + ":" + selectedWay.endMinute;
                } else {
                    message = "Вы прибыли слишком поздно для запланированного времени -> "
                            + selectedWay.endHour + ":" + selectedWay.endMinute;
                }
                builder.setTitle("Подтверждение!")
                        .setMessage(message + "\n" + "Учитывать этот маршрут при корректировке?")
                        .setIcon(R.drawable.voskl_znak)
                        .setCancelable(false)
                        .setNegativeButton("Да, учитывать",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        pushEndedWayIntoHistory(selectedWay, true);
                                    }
                                })
                        .setPositiveButton("Нет, не учитывать",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        dialog.cancel();
                                        pushEndedWayIntoHistory(selectedWay, false);

                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                pushEndedWayIntoHistory(selectedWay, true);
            }
        }
    {
        endWay.setImageResource(R.drawable.kvadrat);
    }

}

    private void pushEndedWayIntoHistory(final CompletedWay selectedWay, final boolean b) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                HistoryWay historyWay = setHistoryWay(selectedWay, b);
                dbHistory.getHistoryWayDao().insertAll(historyWay);
            }
        });
        thread.start();
        try {
            thread.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        delete();
        startActivity(new Intent(getContext(), HistoryWayActivity.class));
    }

    private void setSelectedWay() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    selectedWay = db.getCompletedWayDao().getAllCompletedWays().get(0);
                } catch (Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Активный путь отсутствует", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private HistoryWay setHistoryWay(CompletedWay completedWay, boolean b) {
        HistoryWay historyWay1 = new HistoryWay();
        historyWay1.completed = isCompleted();
        historyWay1.DATE = completedWay.DATE;
        historyWay1.dayStart = completedWay.dayStart;
        historyWay1.dayEnd = completedWay.dayEnd;
        historyWay1.distance = completedWay.distance;
        historyWay1.endHour = completedWay.endHour;
        historyWay1.endMinute = completedWay.endMinute;
        historyWay1.startHour = completedWay.startHour;
        historyWay1.startMinute = completedWay.startMinute;
        historyWay1.startPoint = completedWay.startPoint;
        historyWay1.endMinute = completedWay.endMinute;
        historyWay1.endPoint = completedWay.endPoint;
        if (b)
            historyWay1.wayTime = getWayTime(completedWay);
        else
            historyWay1.wayTime = completedWay.wayTime;
        historyWay1.month = completedWay.month;
        historyWay1.year = completedWay.year;
        historyWay1.minuteToGo = completedWay.minuteToGo;
        historyWay1.missTime = getMissTime(completedWay);
        return historyWay1;
    }

    private int getMissTime(CompletedWay completedWay) {
        long timeEnd = settings.getLong("timeEndWay", 0);
        long timeStart = settings.getLong("timeStartWay", 0);
        return (int) (((timeEnd / 1000 - completedWay.wayTime - timeStart / 1000) / 60));
    }

    private String getMonth(int i) {
        String month = null;
        switch (i) {
            case 1:
                return month = "Января";
            case 2:
                return month = "Февраля";
            case 3:
                return month = "Марта";
            case 4:
                return month = "Апреля";
            case 5:
                return month = "Мая";
            case 6:
                return month = "Июня";
            case 7:
                return month = "Июля";
            case 8:
                return month = "Августа";
            case 9:
                return month = "Сентября";
            case 10:
                return month = "Октября";
            case 11:
                return month = "Ноября";
            case 12:
                return month = "Декабря";
        }
        return month;
    }

    private void startAllAlarms() {
        long fullDate = selectedWay.DATE;
        long timeWay = (long) (selectedWay.wayTime * 1000);
        long fifteen = selectedWay.minuteToGo * 60000;
        long fifteenMinute = fullDate - timeWay - fifteen;
        long fiveMinute = fullDate - timeWay - 300000;
        long startWayTime = fullDate - timeWay;
        long endWayTime = fullDate - (timeWay / 4);

        alarmManager1 = (AlarmManager) (getSystemService(Context.ALARM_SERVICE));
        alarmManager2 = (AlarmManager) (getSystemService(Context.ALARM_SERVICE));
        alarmManager3 = (AlarmManager) (getSystemService(Context.ALARM_SERVICE));
        alarmManager4 = (AlarmManager) (getSystemService(Context.ALARM_SERVICE));
        alarmManager5 = (AlarmManager) (getSystemService(Context.ALARM_SERVICE));

        Intent alarmIntent1 = new Intent(getContext(), AlarmReceiver1.class);
        alarmIntent1.putExtra("first", 1000);
        Intent alarmIntent2 = new Intent(getContext(), AlarmReceiver2.class);
        alarmIntent2.putExtra("second", 2000);
        Intent alarmIntent3 = new Intent(getContext(), AlarmReceiver3.class);
        alarmIntent3.putExtra("third", 3000);
        Intent alarmIntent4 = new Intent(getContext(), AlarmReceiver4.class);
        alarmIntent4.putExtra("four", 4000);
        Intent alarmIntent5 = new Intent(getContext(), AlarmReceiver5.class);
        alarmIntent5.putExtra("five", 5000);


        pendingIntent1 = PendingIntent.getBroadcast(this, 112, alarmIntent1, 0);
        pendingIntent2 = PendingIntent.getBroadcast(this, 1121, alarmIntent2, 0);
        pendingIntent3 = PendingIntent.getBroadcast(this, 2121, alarmIntent3, 0);
        pendingIntent4 = PendingIntent.getBroadcast(this, 3121, alarmIntent4, 0);
        pendingIntent5 = PendingIntent.getBroadcast(this, 4211, alarmIntent5, 0);

        alarmManager1.set(RTC_WAKEUP, System.currentTimeMillis() + 1000, pendingIntent1);
        alarmManager2.set(RTC_WAKEUP, System.currentTimeMillis() + 1000*8, pendingIntent2);
        alarmManager3.set(RTC_WAKEUP, System.currentTimeMillis() + 1000*15, pendingIntent3);
        alarmManager4.set(RTC_WAKEUP, System.currentTimeMillis() + 1000 * 30, pendingIntent4);
        alarmManager5.set(RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60, pendingIntent5);
/*
        alarmManager1.set(RTC_WAKEUP, System.currentTimeMillis(), pendingIntent1);
        alarmManager2.set(RTC_WAKEUP, fifteenMinute, pendingIntent2);
        alarmManager3.set(RTC_WAKEUP, fiveMinute, pendingIntent3);
        alarmManager4.set(RTC_WAKEUP, startWayTime, pendingIntent4);
        alarmManager5.set(RTC_WAKEUP, endWayTime, pendingIntent5);*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (selectedWay != null) {
            getMenuInflater().inflate(R.menu.selected_way, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (selectedWay != null) {

            //noinspection SimplifiableIfStatement
            if (id == R.id.delete) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SelectedWayActivity.this)
                        .setCancelable(true)
                        .setTitle("Подтверждение")
                        .setIcon(R.drawable.dontgoodway)
                        .setMessage("При удалении маршрута, удаляются все уведомления по этому пути")
                        .setPositiveButton("Подтвердить", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                delete();
                                dialogInterface.cancel();
                            }
                        })
                        .setNeutralButton("Отмена", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            }
            if( id == R.id.inActive)
            {
                pushToWay(selectedWay);
            }
            if(id == R.id.refresh)
            {
                setImagesStep();
                setVisibleButton();
            }
        }


        return super.onOptionsItemSelected(item);
    }

    private void pushToWay(final CompletedWay selectedWay) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Way way = setWay(selectedWay);
                dbWay.getWayDao().insertAll(way);            }
        });
        thread.start();
        try {
            thread.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        delete();
        startActivity(new Intent(getContext(), FutureWaysActivity.class));
    }
    private Way setWay(CompletedWay completedWay) {
        Way way = new Way();
        way.DATE=           completedWay.DATE;
        way.dayStart=       completedWay.dayStart;
        way.dayEnd=         completedWay.dayEnd;
        way.distance=       completedWay.distance;
        way.endHour=        completedWay.endHour;
        way.endMinute=      completedWay.endMinute;
        way.startHour=      completedWay.startHour;
        way.startMinute=    completedWay.startMinute;
        way.startPoint=     completedWay.startPoint;
        way.endMinute=      completedWay.endMinute;
        way.endPoint=       completedWay.endPoint;
        way.wayTime=        completedWay.wayTime;
        way.month=          completedWay.month;
        way.year=           completedWay.year;
        way.textSpinner=    completedWay.textSpinner;
        way.minuteToGo=     completedWay.minuteToGo;
            return way;
    }
    private void delete() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                db.getCompletedWayDao().delete(db.getCompletedWayDao().getAllCompletedWays()
                        .get(db.getCompletedWayDao().getAllCompletedWays().size() -1));
            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean("ImageFirstNotif", false);
        editor.putBoolean("ImageSecondNotif", false);
        editor.putBoolean("ImageThirdNotif", false);
        editor.putBoolean("ImageFourNotif", false);
        editor.putBoolean("ImageFiveNotif", false);
        editor.putBoolean("startButton",false);
        editor.putBoolean("finishButton",false);
        editor.apply();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            try {
                notificationManager.cancel(1);
            }
            catch (Exception e)
            {

            }
            try {
                notificationManager.cancel(2);
            }
            catch (Exception e)
            {

            }
            try {
                notificationManager.cancel(3);
            }
            catch (Exception e)
            {

            }
            try {
                notificationManager.cancel(4);
            }
            catch (Exception e)
            {

            }
            try {
                notificationManager.cancel(5);
            }
            catch (Exception e)
            {

            }
        }

        try {
            alarmManager1.cancel(pendingIntent1);
            pendingIntent1.cancel();
        } catch (Exception e) {
            Log.e("Error", "ERROR_WHEN_DELETE_ALARM_1");
        }
        try {
            alarmManager2.cancel(pendingIntent2);
            pendingIntent2.cancel();
        } catch (Exception e) {
            Log.e("Error", "ERROR_WHEN_DELETE_ALARM_2");
        }
        try {
            alarmManager3.cancel(pendingIntent3);
            pendingIntent3.cancel();
        } catch (Exception e) {
            Log.e("Error", "ERROR_WHEN_DELETE_ALARM_3");
        }
        try {
            alarmManager4.cancel(pendingIntent4);
            pendingIntent4.cancel();
        } catch (Exception e) {
            Log.e("Error", "ERROR_WHEN_DELETE_ALARM_4");
        }
        try {
            alarmManager5.cancel(pendingIntent5);
            pendingIntent5.cancel();
        } catch (Exception e) {
            Log.e("Error", "ERROR_WHEN_DELETE_ALARM_5");
        }
        startActivity(new Intent(getContext(), FutureWaysActivity.class));
        finish();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.show_future_ways) {
            startActivity(new Intent(this, FutureWaysActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.create_new_way) {
            startActivity(new Intent(this, WebMapsActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.show_history_ways) {
            startActivity(new Intent(this, HistoryWayActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.selected_way) {
            startActivity(new Intent(this, SelectedWayActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
        else if (id == R.id.info)
        {
            startActivity(new Intent(this, InfoActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Context getContext() {
        return context;
    }

    public boolean isCompleted() {
        return true;
    }

    public double getWayTime(CompletedWay completedWay) {
        double r = (long) completedWay.wayTime;
        int e = getMissTime(completedWay);
        double d;
        double wayTime;
        if (e > 0) {
            d = (long) (completedWay.wayTime + getMissTime(completedWay) * 60 + 120);
        } else {
            d = (long) (completedWay.wayTime + getMissTime(completedWay) * 60 + 120);
        }
        if(e>-10 && e<0)
        {
            wayTime = r;
        }
        else {
            wayTime =(r+d+r)/3;
        }
        return wayTime;
    }
}


