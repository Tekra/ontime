package com.example.tekra.ontime.SaveWays;

// для записи объекта получаемого из карты
class DirectionsGeocodedWaypoint extends DirectionsResult {
    public String geocoder_status;
    public boolean partial_match;
    public String place_id;
    public String[] types;
}
