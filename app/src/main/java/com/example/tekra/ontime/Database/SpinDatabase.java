package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Spin.class}, version = 1)
public abstract class SpinDatabase extends RoomDatabase {
    public abstract SpinDao getSpinnerDao();
}
