package com.example.tekra.ontime.Notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;
import com.example.tekra.ontime.Activitys.SelectedWayActivity;
import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;

public class PushService1 extends IntentService {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    public SharedPreferences settings;
    private CompletedWay listForAlarm;
    private CompletedWayDatabase db;
    private int number;


    public PushService1() {
        super("");
    }

    public PushService1(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);

        number = intent.getIntExtra("number", 1);
        db = Room.databaseBuilder(getApplicationContext(),
                CompletedWayDatabase.class, "database5").build(); //
        //  инициализация БД
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    listForAlarm = db.getCompletedWayDao().getAllCompletedWays()
                            .get(db.getCompletedWayDao().getAllCompletedWays().size() - 1);

                }
            });
            thread.start();
            try {
                thread.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            if (intent != null) {
                sendNotification(number);
            }


        }
        catch (Exception e)
        {
            Log.e("Error service", "NullSelectedWay");
        }

        }

    private void sendNotification(int i) {

//String channelId = getString(R.string.default_notification_channel_id);
        switch (i) {
            case 1000: {
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putBoolean("ImageFirstNotif",true);
                editor.apply();
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.logo)
                                .setContentTitle("Уведомление о начале маршрута добавлено")
                                .setSound(defaultSoundUri)
                                .setVibrate(new long[]{0, 500, 0})
                                .setContentText("Начало пути в -> " + getZero(listForAlarm.startHour)+listForAlarm.startHour + ":" +
                                    getZero( listForAlarm.startMinute)+    listForAlarm.startMinute+ " " + listForAlarm.dayStart + " " + getMonth(listForAlarm.month))
                                .setAutoCancel(true);


                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(1,
                        notificationBuilder.build());
                stopSelf();
                return;
            }


        }
    }

    private String getMonth(int i) {
        String month = null;
        switch (i) {
            case 1:
                return month = "Января";
            case 2:
                return month = "Февраля";
            case 3:
                return month = "Марта";
            case 4:
                return month = "Апреля";
            case 5:
                return month = "Мая";
            case 6:
                return month = "Июня";
            case 7:
                return month = "Июля";
            case 8:
                return month = "Августа";
            case 9:
                return month = "Сентября";
            case 10:
                return month = "Октября";
            case 11:
                return month = "Ноября";
            case 12:
                return month = "Декабря";
        }
        return month;
    }
    private String getZero(int i) {
        if (i < 10) {
            return "0";
        } else {
            return "";
        }
    }
    @Override
    public void onDestroy() {

        super.onDestroy();

    }


}