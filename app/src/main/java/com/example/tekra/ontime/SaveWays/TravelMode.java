package com.example.tekra.ontime.SaveWays;

// для записи объекта получаемого из карты
class TravelMode {
    private static final String bicycling = "BICYCLING";
    private static final String driving = "DRIVING";
    private static final String transit = "TRANSIT";
    private static final String walking = "WALKING";
}
