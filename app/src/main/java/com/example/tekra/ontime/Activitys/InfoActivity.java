package com.example.tekra.ontime.Activitys;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.tekra.ontime.R;

public class InfoActivity extends AppCompatActivity {
    TextView textView0;
    TextView textView1;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    TextView textView5;
    TextView textView6;
    TextView textView7;
    TextView textView8;
    TextView textView9;
    TextView textView10;
    TextView textView11;
    TextView textView12;
    TextView textView13;
    TextView textView14;
    TextView textView15;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textView0 = findViewById(R.id.create_way_title);
        textView1 = findViewById(R.id.create_way_text);
        textView2 = findViewById(R.id.select_date_and_time_title);
        textView3 = findViewById(R.id.select_date_and_time_text);
        textView4 = findViewById(R.id.spinner_title);
        textView5 = findViewById(R.id.spinner_text);
        textView6 = findViewById(R.id.future_way_title);
        textView7 = findViewById(R.id.future_way_text);
        textView8 = findViewById(R.id.active_way_title);
        textView9 = findViewById(R.id.active_way_text);
        textView10 = findViewById(R.id.notif_title);
        textView11 = findViewById(R.id.notif_text);
        textView12 = findViewById(R.id.hisory_way_title);
        textView13 = findViewById(R.id.hisory_way_text);
        textView14 = findViewById(R.id.info_about_program_title);
        textView15 = findViewById(R.id.info_about_program_text);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    }

