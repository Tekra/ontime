package com.example.tekra.ontime.SaveWays;

import com.google.android.gms.maps.model.LatLng;

// для записи объекта получаемого из карты
class DirectionsLeg {
    public DirectionsStep[] steps;
    public Distance distance;
    public Duration duration;
    public LatLng start_location;
    public LatLng end_location;
    public String start_address;
    public String end_address;

}
