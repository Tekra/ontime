package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
@Dao
public interface HistoryWayDao {
    @Insert
    void insertAll(HistoryWay... historyWays);

    // Удаление Person из бд
    @Delete
    void delete(HistoryWay historyWay);

    @Delete
    void deleteOne(HistoryWay historyWay);

    // Получение всех Person из бд
    @Query("SELECT * FROM  historyway")
    List<HistoryWay> getAllHistoryWays();

    // Получение всех Person из бд с условием

    @Insert
    void insertAll(List<HistoryWay> allHistoryWays);
}
