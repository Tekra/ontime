package com.example.tekra.ontime.Activitys;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tekra.ontime.Database.HistoryWay;
import com.example.tekra.ontime.Database.HistoryWayDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;

import java.util.List;

public class DataActivity extends AppCompatActivity {


    private static int n;
    public TextView textView;
    Intent intent2;
    Intent intent;
    FutureWaysActivity f = new FutureWaysActivity();
    private WayDatabase db;
    private Context context;
    private int flagNotif;
    private boolean wayAgoBool = false;
    private HistoryWay lastListUser;
    private HistoryWayDatabase dbHistory;

    public WayDatabase getDB() {
        return db;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        textView = findViewById(R.id.textView);
        context = this;
        intent = getIntent();
        dbHistory = Room.databaseBuilder(getApplicationContext(),
                HistoryWayDatabase.class, "database4").build();// инициализация БД
        db = Room.databaseBuilder(getApplicationContext(),
                WayDatabase.class, "database15").build();// инициализация БД
        Button button = findViewById(R.id.button);
        Button button2 = findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveToBD();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent2);
            }
        });
    }

    // Когда переходит в это активити, то данные сохраняются
    // и Activity завершается
    @Override
    protected void onResume() {
        super.onResume();
        saveToBD(); // сохранение в БД

        flagNotif = intent.getIntExtra("notifFlag", 1);
        int position = intent.getIntExtra("position", 1);
        intent2 = new Intent(this, FutureWaysActivity.class);
        intent2.putExtra("notifFlag", flagNotif);
        intent2.putExtra("positionSpin", position);
        startActivity(intent2);
        finish();
    }

    public Context getContext() {
        return context;
    }

    public void saveToBD() {
        WorkingClass workingClass = new WorkingClass();
        Thread thread = new Thread(workingClass); //Новый поток т.к запись не должна быть в UI потоке
        thread.start();
    }

    private int getDayAtMonth(int month, int year) {
        int day = 0;
        switch (month) {

            case 1: {
                return day = 31;
            }
            case 2: {
                if (year == 2020 || year == 2024 || year == 2028)
                    return day = 29;
                else return day = 28;
            }
            case 3: {
                return day = 31;
            }
            case 4: {
                return day = 30;
            }
            case 5: {
                return day = 31;
            }
            case 6: {
                return day = 30;
            }
            case 7: {
                return day = 31;
            }
            case 8: {
                return day = 31;
            }
            case 9: {
                return day = 30;
            }
            case 10: {
                return day = 31;
            }
            case 11: {
                return day = 30;
            }
            case 12: {
                return day = 31;
            }
        }
        return day;
    }

    private int getStartMinute(int endHour, int endMinute, double wayTime) {
        int minuteInWay = (int) (wayTime / 60);
        int timeEndInMinute = endHour * 60 + endMinute;
        switch (endHour) {
            case 1: {
                return 1;
            }

        }
        if (timeEndInMinute - minuteInWay > 0) {
            int hour = (int) (timeEndInMinute - minuteInWay) / 60;
            int fullMinute = timeEndInMinute - minuteInWay;
            return fullMinute - (hour * 60);
        } else {
            int hour = (int) (minuteInWay - timeEndInMinute) / 60;
            int fullMinute = minuteInWay - timeEndInMinute;
            return 60 - (fullMinute - (hour * 60));
        }
    }

    private int getStartHour(int endHour, int endMinute, double wayTime, Way way) {
        int minuteInWay = (int) (wayTime / 60);
        int timeEndInMinute = endHour * 60 + endMinute;
        if (timeEndInMinute - minuteInWay > 0) {
            int hour = (int) (timeEndInMinute - minuteInWay) / 60;
            return hour;
        } else {
            int hour = (int) (minuteInWay - timeEndInMinute) / 60;
            wayAgoBool = true;
            return 23 - hour;
        }
    }

    class WorkingClass implements Runnable {
        private String dataForText;
        private String dataForText1;
        private String dataForText2;
        private String dataForText3; // поля для тестов
        private String dataForText4;
        private String dataForText5;
        private String dataForText6;
        private double hours;
        private double minute;
        private double distance;
        private int n1; // Primary Key
        private Way DB;


        @Override
        public void run() {
            intent = getIntent();
            List<HistoryWay> list = dbHistory.getHistoryWayDao().getAllHistoryWays();
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).startPoint.equals(intent.getStringExtra("origin"))
                        && list.get(i).endPoint.equals(intent.getStringExtra("destination"))) {
                    lastListUser = list.get(i);
                }
            }


            db = getDB();
            Way way = new Way();
            n1 = way.Number;
           /* if (lastListUser != nullj) {
                way.minuteToGo = lastListUser.minuteToGo;
                way.wayTime = lastListUser.wayTime;
            }*/
            if(lastListUser!=null)
            {
               way.wayTime = lastListUser.wayTime;
            }
            else
            {
                way.wayTime = intent.getDoubleExtra("duration", 10.0); // время маршрута
            }
            way.completed = null; //статус завершения
            way.startPoint = intent.getStringExtra("origin"); // начальная точка
            way.endPoint = intent.getStringExtra("destination"); //конечная точка
            way.missTime = 3; //время опаздания
            way.DATE = intent.getLongExtra("Time", 5);
            way.minuteToGo = 15; // время для продварительныз сборов (для уведомлений)

            way.distance = intent.getDoubleExtra("distance", 10.0); //дистанция маршрута
            way.dayEnd = intent.getIntExtra("day", 1); // день когда будет совершен
            way.month = intent.getIntExtra("month", 1);//месяц когда будет совершен
            way.year = intent.getIntExtra("year", 1);//год когда будет совершен
            way.endHour = intent.getIntExtra("hoursWay", 10);//час прибытия
            way.endMinute = intent.getIntExtra("minuteWay", 10);//минута прибытия
            way.textSpinner = intent.getStringExtra("textSpinner");
            hours = Math.round(way.wayTime / 60 / 60);
            minute = Math.round(way.wayTime / 60);
            Way newWay = way;
            way.dayStart = newWay.dayEnd;
            way.startHour = getStartHour(way.endHour, way.endMinute, way.wayTime, newWay);
            way.startMinute = getStartMinute(way.endHour, way.endMinute, way.wayTime);
            if (wayAgoBool) {
                if (way.dayStart- 1 == 0) {
                    if (way.month - 1 == 0) {
                        way.year = way.year - 1;
                        way.month = 12;
                        way.dayStart = 31;
                    } else {
                        way.month = way.month - 1;
                        way.dayStart = getDayAtMonth(way.month, way.year);
                    }

                } else {
                    way.dayStart = way.dayStart - 1;
                }
            }
            db.getWayDao().insertAll(way);
        }




        /*
         *
         *
         * поля для теста
         *
         *
         * */

/*

            DB = db.getWayDao().getAllWays().get(n1);
            hours = Math.round(DB.wayTime / 60 / 60);
            minute = Math.round(DB.wayTime / 60);

            distance = (int) DB.distance / 100;
            dataForText = DB.startPoint;
            dataForText1 = DB.endPoint;
            dataForText2 = (int) DB.startHour+ " Hours " + (int) DB.endMinute + " Minutes";
            dataForText3 = String.valueOf(DB.completed);
            dataForText4 = String.valueOf(DB.Number) + "NUM";
            dataForText5 = String.valueOf(distance / 10) + "km";
            dataForText6 = DB.day + " day " + DB.month + " month " + DB.year + "year "
                    + "\n" + DB.endHour + " hours " + DB.endMinute + " minute";
*/

        //Фоновая работа
        //Отправить в UI поток новый Runnable
// Сделать все активити
        //Сделать все ссылки


           /* DataActivity.this.runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    textView.setText(textView.getText().toString() + "\n" + dataForText4
                            + "\n" + dataForText
                            + "\n" + dataForText1
                            + "\n" + dataForText2
                            + "\n" + dataForText3
                            + "\n" + dataForText5
                            + "\n" + dataForText6);
                }
            });*/
    }
}

