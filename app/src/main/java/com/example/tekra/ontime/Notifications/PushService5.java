package com.example.tekra.ontime.Notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.tekra.ontime.Activitys.SelectedWayActivity;
import com.example.tekra.ontime.R;

public class PushService5 extends IntentService {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    private int number;
    private SharedPreferences settings;

    public PushService5() {
        super("");
    }

    public PushService5(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);
        number = intent.getIntExtra("number", 1);

        if (intent != null) {
            sendTime(number);
        }
    }

    private void sendTime(int i) {


        if( i == 5000){
            SharedPreferences.Editor editor;
            editor = settings.edit();
            editor.putBoolean("finishButton", true);
            editor.apply();
            Intent alarmIntent1 = new Intent(getApplicationContext(), SelectedWayActivity.class);
            alarmIntent1.putExtra("end", true);
            alarmIntent1.putExtra("timeEnd", true);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 105,
                    alarmIntent1, PendingIntent.FLAG_CANCEL_CURRENT);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle("Конец маршрута")
                            .setContentText("Нажмите, чтобы закончить маршрут")
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent)
                            .setVibrate(new long[]{0, 500, 0})
                            .addAction(R.drawable.baseline_navigate_next_white_18dp, "Финиш", pendingIntent)
                            .setAutoCancel(true);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(5,
                    notificationBuilder.build());
            stopSelf();
            return;
        }
    }
}
