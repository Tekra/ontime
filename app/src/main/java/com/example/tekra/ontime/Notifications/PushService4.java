package com.example.tekra.ontime.Notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;
import com.example.tekra.ontime.Activitys.SelectedWayActivity;
import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.R;

public class PushService4 extends IntentService {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    private int number;
    private SharedPreferences settings;

    public PushService4() {
        super("");
    }

    public PushService4(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);
        number = intent.getIntExtra("number", 1);

        if (intent != null) {
            sendTime(number);
        }
    }

    private void sendTime(int i) {

//String channelId = getString(R.string.default_notification_channel_id);


        if (i ==4000) {
            SharedPreferences.Editor editor;
            editor = settings.edit();
            editor.putBoolean("startButton", true);
            editor.apply();
            Intent alarmIntent1 = new Intent(getApplicationContext(), SelectedWayActivity.class);
            alarmIntent1.putExtra("start", true);
            alarmIntent1.putExtra("timeStart", true);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 998,
                    alarmIntent1, PendingIntent.FLAG_CANCEL_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.logo)
                            .setAutoCancel(true)
                            .setContentTitle("Начало пути")
                            .setSound(defaultSoundUri)
                            .setVibrate(new long[]{0, 500, 0})
                            .addAction(R.drawable.baseline_navigate_next_white_18dp, "Старт", pendingIntent)
                            .setContentText("Нажмите, чтобы начать маршрут")
                            .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(4,
                    notificationBuilder.build());
            stopSelf();
            return;
        }
    }

}
