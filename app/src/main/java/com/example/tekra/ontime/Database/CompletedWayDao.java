package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;
@Dao
public interface CompletedWayDao {
    // Добавление Person в бд
    @Insert
    void insertAll(CompletedWay... completedWays);

    // Удаление Person из бд
    @Delete
    void delete(CompletedWay completedWay);

    @Delete
    void deleteOne(CompletedWay completedWay);

    // Получение всех Person из бд
    @Query("SELECT * FROM completedway")
    List<CompletedWay> getAllCompletedWays();

    // Получение всех Person из бд с условием

    @Insert
    void insertAll(List<CompletedWay> allCompletedWays);
}
