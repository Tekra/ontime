package com.example.tekra.ontime.Activitys;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.example.tekra.ontime.JavaScript.MyJavaScriptInterface;
import com.example.tekra.ontime.JavaScript.UserInfoScript;
import com.example.tekra.ontime.R;
import com.example.tekra.ontime.SaveWays.DirectionsResult;

public class WebMapsActivity extends AppCompatActivity {


    private final Bitmap favicon = null;
    FloatingActionButton fab;
    WebView webView;
    Context context;
    private DirectionsResult response;
    private UserInfoScript userInfoScript;
    private WebView webViewForGif;
    private ImageView loading;
    private WebMapsActivity wb;
    private String origin;
    private String destination;
    private double duration;
    private double distance;

    public void setWb(WebMapsActivity wb) {
        this.wb = wb;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public Context getContext() {
        return context;
    }

    @SuppressLint({"SetJavaScriptEnabled", "JavascriptInterface"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_web_maps);
        webView = findViewById(R.id.webView);
        context = getApplicationContext();
        loading = findViewById(R.id.imageViewLoading);

        final ProgressDialog pd = new ProgressDialog(WebMapsActivity.this);
        pd.setTitle("Подождите");
        pd.setMessage("Загрузка...");
        // добавляем кнопку


        webView.getSettings().setJavaScriptEnabled(true);
        // использование JavaScript на webView
        webView.addJavascriptInterface(new MyJavaScriptInterface(context), "Android");
       /* Scripto scripto = new Scripto.Builder(webView).build();
        scripto.addInterface("Upload", new PreferencesInterface(this));
        scripto.onPrepared(new ScriptoPrepareListener() {
            @Override
            public void onScriptoPrepared() {

            }
        });
        userInfoScript = scripto.create(UserInfoScript.class);

        scripto.onPrepared(new ScriptoPrepareListener() {
            @Override
            public void onScriptoPrepared() {
                userInfoScript.loadUserData();
            }
        });*/
        fab = (FloatingActionButton) findViewById(R.id.fab9);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // запись данных в Activity
                setTextDATA();
                // запись даднных в intent
                Intent intent = new Intent(getApplicationContext(), CalendarActivity.class);
                intent.putExtra("origin", origin);

                intent.putExtra("destination", destination);

                intent.putExtra("duration", duration);

                intent.putExtra("distance", distance);
                long dayInMillis   = 24 * 60 * 60 * 1000;
                if ((origin == null) || (destination == null) || (distance == 0) || (duration==0) || (duration> dayInMillis))
                {
                    Snackbar.make(webView, "Не удалось обработать запрос", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else {
                    startActivity(intent);
                }
            }
        });
        fab.setVisibility(View.INVISIBLE);
         loading.setVisibility(View.INVISIBLE);
         webView.loadUrl("file:///android_asset/WayToFuture");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(webView, url);

               loading.setVisibility(View.INVISIBLE);
                fab.setVisibility(View.VISIBLE);
                pd.cancel();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {

                super.onPageStarted(view, url, favicon);
                loading.setVisibility(View.VISIBLE);
                fab.setVisibility(View.INVISIBLE);
                pd.show();

            }

        });


    }


   /* public void getUserData(View view) {
        userInfoScript.loadUserData();
    }*/

    @Override
    protected void onResume() {
        super.onResume();
    }
    //Запись данных в Activity
    public void setTextDATA() {
        MyJavaScriptInterface myJava =
                new MyJavaScriptInterface(this);
        setOrigin(myJava.getOrigin());
        setDestination(myJava.getDestination());
        setDistance(myJava.getDistance());
        setDuration(myJava.getDuration());

    }
    @Override
    public void onBackPressed()
    {
        startActivity(new Intent(getApplicationContext(),FutureWaysActivity.class));
    }


}




