package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {CompletedWay.class}, version = 5)
public abstract class CompletedWayDatabase extends RoomDatabase {
    public abstract CompletedWayDao getCompletedWayDao();
}
