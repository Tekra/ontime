package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface WayDao {

    // Добавление Person в бд
    @Insert
    void insertAll(Way... ways);

    // Удаление Person из бд
    @Delete
    void delete(Way way);

    @Delete
    void deleteOne(Way way);

    // Получение всех Person из бд
    @Query("SELECT * FROM way")
    List<Way> getAllWays();

    // Получение всех Person из бд с условием
    @Query("SELECT * FROM way WHERE wayTime IS 1")
    List<Way> getAllGoodTime();

    @Insert
    void insertAll(List<Way> allWays);

}