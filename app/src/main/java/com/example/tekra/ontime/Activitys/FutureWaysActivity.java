package com.example.tekra.ontime.Activitys;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;
import com.example.tekra.ontime.RecyclerView.WayAdapterFuture;
import com.example.tekra.ontime.RecyclerView.WayModelFuture;

import java.util.ArrayList;
import java.util.List;

public class FutureWaysActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, WayAdapterFuture.OnItemClickListener {
    private static final int NOTIFY_ID = 101;
    public static final String ON_TIME_SETTINGS = "OnTimeSettings";

    public static int notifFlag;
    static Context context;
    private static PendingIntent alarmIntentRTC;

  /*  public void setCountinue(int flag, Way seviceList) {
        continueAlarm(flag,seviceList);
    }*/
    public final int zeroFlag = 0;
    public final int firstFlag = 1000;
    public final int secondFlag = 1000;
    public final int thriedFlag = 2000;
    public final int fourthFlag = 3000;
    public final int fifthFlag = 4000;
    public final int sixthFlag = 5000;
    public final int seventhFlag = 6000;
    private final int FLAG = 8000;
    public List<Way> list;
    public int position;
    AlertDialog alertDialog;
    FloatingActionButton fab;
    PendingIntent pendingIntent1;
    PendingIntent pendingIntent2;
    PendingIntent pendingIntent3;
    Button buttonStart;
    RecyclerView recyclerView;
    Intent notificationIntent;
    Intent intentNotif;
    private int flagNotif;
    private List<Way> listForNotification;
    private Way listForAlarm;
    private WayAdapterFuture wayAdapterFuture;
    private ArrayList<WayModelFuture> ways = new ArrayList<>();
    private WayDatabase db;
    private CompletedWayDatabase dbCompletedWay;
    private SharedPreferences settings;
    private ImageView nullWays;

    public static Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intentNotif = getIntent();
        createPreferences();
        position = intentNotif.getIntExtra("positionSpin", 1);
        setContentView(R.layout.activity_future_ways);

        dbCompletedWay  = Room.databaseBuilder(getApplicationContext(),
                CompletedWayDatabase.class,"database5").build();
        db = Room.databaseBuilder(getApplicationContext(),
                WayDatabase.class, "database15").build(); // инициализация БД
        // запись в list из БД
        fab = findViewById(R.id.fab121);

        context = getApplicationContext();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        nullWays = findViewById(R.id.nullWayImage);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        wayAdapterFuture = new WayAdapterFuture();

        wayAdapterFuture.setOnItemClickListener(this);

        recyclerView.setAdapter(wayAdapterFuture);
        getListWays();

    }

    private void getMin() {
        long min = 9999999999999L;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).DATE < min) {
                min = list.get(i).DATE;
            }
        }


    }



    public void setList(Way list) {
        listForAlarm = list;
    }

    // Когда совершается переход в Activity - все обнвляется
    @Override
    protected void onResume() {
        super.onResume();
        pushInfoActiveWay();
        getListWays();
        // setListForAlarm();
        new CountDownTimer(200, 200) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                getAllModels();
            }
        };

    }

    private void loadFromDatabase() {
        ways = getAllModels(); // запись в ways ArrayList<WayModelFuture>
        wayAdapterFuture.updateNotes(ways); //обновления адаптера
    }

    @SuppressLint("SetTextI18n")
    private ArrayList<WayModelFuture> getAllModels() {

        ArrayList<WayModelFuture> waysModel = new ArrayList<>(); // массив
        ArrayList<WayModelFuture> newWaysModel = new ArrayList<>(); //обратный ему массив
        if (list.size()== 0)
        {
            nullWays.setVisibility(View.VISIBLE);
            nullWays.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(),WebMapsActivity.class));
                }
            });
            fab.setVisibility(View.INVISIBLE);
        }
        else
        {
            nullWays.setVisibility(View.INVISIBLE);
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getApplicationContext(),WebMapsActivity.class));
                }
            });
        }

        if (list != null) { // если Ways есть, то
            for (int i = 0; i < list.size(); i++) {
                // Если путь завершен, то записываю в Историю
                String startPoint = list.get(i).startPoint;
                String endPoint = list.get(i).endPoint;
                String timeStart = getZero(list.get(i).startHour) + list.get(i).startHour
                        + ":" + getZero(list.get(i).startMinute) + list.get(i).startMinute;
                String timeEnd = getZero(list.get(i).endHour) + list.get(i).endHour
                        + ":" + getZero(list.get(i).endMinute) + list.get(i).endMinute;
                String dateStart = list.get(i).dayStart + " " + getMonth(list.get(i).month);
                String dateEnd = list.get(i).dayEnd + " " + getMonth(list.get(i).month);

                WayModelFuture wayModelFuture = new WayModelFuture();
                wayModelFuture.setStartPoint(startPoint);
                wayModelFuture.setEndPoint(endPoint);
                wayModelFuture.setTimeStart(timeStart+"");
                wayModelFuture.setTimeEnd(timeEnd);
                wayModelFuture.setDateStart(dateStart);
                // Обработка отрицательных значений времени
                // Обработка отрицательных значений времени
                if (startPoint != null && endPoint != null) {
                    waysModel.add(wayModelFuture);
                }
                else {
                    Toast.makeText(getContext(),"Не удалось обработать маршрут",Toast.LENGTH_LONG).show();
                    final int finalI = i;
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            db.getWayDao().delete(db.getWayDao().getAllWays().get(finalI));
                        }

                    });
                    thread.start();
                    try {
                        thread.join(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }// scheduleRepeatingRTCNotification(this,i); Установка на каждую модель уведовления*/
            }
            // переворот массива wayModel, чтобы вверзу оказались последние добавления пользователя
            for (int i = waysModel.size(); i > 0; i--) {
                newWaysModel.add(waysModel.get(i - 1));

            }


        }
        //TODO
        // Если Ways пуст, то TextView посередине "Нет ближайщих маршрутов"
        //ПОКА ВЫВОДИТСЯ TOAST

        return newWaysModel;
    }

    private String getZero(int i) {
        if (i < 10) {
            return "0";
        } else {
            return "";
        }
    }

    // TODO
    // перевод всего времени в long для дальнейшей сортировки
    // по ближайему к реальному времени событию
   /* public long getValueDate(int day, int month, int hour, int minute) {
        long time = System.currentTimeMillis();
        long returnTime = month * 30 * 86400000L + day * 86400000L + hour * 3600000 + minute * 60000;
        return returnTime - time;
    }*/

    // Обработка отрицательных значений времени

    // Получение и запись в list данных из БД
    private void getListWays() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                list = db.getWayDao().getAllWays();

            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loadFromDatabase();
    }

    // Parse название месяца от числа
    private String getMonth(int i) {
        String month = null;
        switch (i) {
            case 1:
                return month = "Января";
            case 2:
                return month = "Февраля";
            case 3:
                return month = "Марта";
            case 4:
                return month = "Апреля";
            case 5:
                return month = "Мая";
            case 6:
                return month = "Июня";
            case 7:
                return month = "Июля";
            case 8:
                return month = "Августа";
            case 9:
                return month = "Сентября";
            case 10:
                return month = "Октября";
            case 11:
                return month = "Ноября";
            case 12:
                return month = "Декабря";
        }
        return month;
    }

    // Parse падеж минуты от числа
    private String getMinute(int i) {
        String minute = null;
        while (i > 10) {
            i = i % 10;
        }
        switch (i) {
            case 1:
                return minute = "минута";
            case 2:
                return minute = "минуты";
            case 3:
                return minute = "минуты";
            case 4:
                return minute = "минуты";
            case 5:
                return minute = "минут";
            case 6:
                return minute = "минут";
            case 7:
                return minute = "минут";
            case 8:
                return minute = "минут";
            case 9:
                return minute = "минут";
            case 0:
                return minute = "минут";
        }
        return " " + minute;
    }

    // Parse падеж часа от числа
    private String getHours(int i) {
        String hours = null;
        while (i > 10) {
            i = i % 10;
        }
        switch (i) {
            case 1:
                return hours = "Час";
            case 2:
                return hours = "Часа";
            case 3:
                return hours = "Часа";
            case 4:
                return hours = "Часа";
            case 5:
                return hours = "Часов";
            case 6:
                return hours = "Часов";
            case 7:
                return hours = "Часов";
            case 8:
                return hours = "Часов";
            case 9:
                return hours = "Часов";
            case 0:
                return hours = "Часов";
        }
        return " " + hours;
    }


    public void createPreferences() {
        settings = getSharedPreferences(ON_TIME_SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = settings.edit();
        ed.putInt("defaultTimeToGo",15);
        ed.apply();
    }
    // Скрыть шторку
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    // Установка навигации
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.show_future_ways) {
            startActivity(new Intent(this, FutureWaysActivity.class), ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.create_new_way) {
            startActivity(new Intent(this, WebMapsActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.show_history_ways) {
            startActivity(new Intent(this, HistoryWayActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());

        } else if (id == R.id.selected_way) {
            startActivity(new Intent(this, SelectedWayActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        } else if (id == R.id.info)
        {
            startActivity(new Intent(this, InfoActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //TODO
    // По нажатию сделать диалоговое окно
    // СЕЙЧАС - УДАЛЕНИЕ
    @Override
    public void onItemClick(View view, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FutureWaysActivity.this);
        builder.setTitle("Выберите действие")
                .setIcon(R.drawable.todosomething)
                .setPositiveButton("Активный путь", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Thread th = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                CompletedWay completedWay =  setCompletedWay(db.getWayDao().getAllWays().get(db.getWayDao().getAllWays().size()-position-1));
                                try {
                                    dbCompletedWay.getCompletedWayDao().delete(dbCompletedWay.getCompletedWayDao().getAllCompletedWays().get(0));
                                }
                                catch (Exception e)
                                {
                                    Log.e("error Completed way","CompletedWays is null");
                                }
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putBoolean("ImageFirstNotif", false);
                                editor.putBoolean("ImageSecondNotif", false);
                                editor.putBoolean("ImageThirdNotif", false);
                                editor.putBoolean("ImageFourNotif", false);
                                editor.putBoolean("ImageFiveNotif", false);
                                editor.putBoolean("startButton",false);
                                editor.putBoolean("finishButton",false);
                                editor.apply();
                                dbCompletedWay.getCompletedWayDao().insertAll(completedWay);
                                db.getWayDao().delete(db.getWayDao().getAllWays().get(db.getWayDao().getAllWays().size()-position-1));
                                Intent intent = new Intent(getContext(), SelectedWayActivity.class);
                                intent.putExtra("bool", true);
                                intent.putExtra("pos", position);
                                startActivity(intent);
                            }
                        });
                        th.start();
                        try {
                            th.join(3000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton("Удалить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        Thread th = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                db.getWayDao().delete(db.getWayDao().getAllWays().get(db.getWayDao().getAllWays().size()-position-1));
                            }
                        });
                        th.start();
                        try {
                            th.join(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        dialog.cancel();
                        wayAdapterFuture.notifyItemRemoved(position);

                        new CountDownTimer(170, 170) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                try {
                                    ways.remove(position);
                                    wayAdapterFuture.updateNotes(ways);
                                    getListWays();
                                    getAllModels();
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }.start();




                    }
                })
        .setNeutralButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();


    }

    public CompletedWay setCompletedWay(Way way) {
        CompletedWay completedWay = new CompletedWay();
        completedWay.DATE = way.DATE;
        completedWay.dayStart = way.dayStart;
        completedWay.dayEnd = way.dayEnd;
        completedWay.distance =way.distance;
        completedWay.endHour = way.endHour;
        completedWay.endMinute = way.endMinute;
        completedWay.startHour = way.startHour;
        completedWay.startMinute = way.startMinute;
        completedWay.startPoint = way.startPoint;
        completedWay.endMinute = way.endMinute;
        completedWay.endPoint = way.endPoint;
        completedWay.wayTime = way.wayTime;
        completedWay.month = way.month;
        completedWay.year = way.year;
        completedWay.textSpinner = way.textSpinner;
        completedWay.minuteToGo = way.minuteToGo;
        return completedWay;
    }
    public void pushInfoActiveWay()
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
               if( dbCompletedWay.getCompletedWayDao().getAllCompletedWays().size() == 0 && db.getWayDao().getAllWays().size() != 0)
               {
                   runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           Snackbar.make(recyclerView, "Нажмите на маршрут, чтобы сделать активным", Snackbar.LENGTH_LONG)
                                   .setAction("Action", null).show();
                       }
                   });
               }
            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
