package com.example.tekra.ontime.RecyclerView;

import com.example.tekra.ontime.R;

public class WayModelHistory {

    private String pointStart;
    private String pointEnd;
    private String dateStart;
    private String timeEnd;
    private String missTime;
    private boolean isGoodWay;

    public String getPointStart() {
        return pointStart;
    }

    public void setPointStart(String pointStart) {
        this.pointStart = pointStart;
    }

    public String getPointEnd() {
        return pointEnd;
    }

    public void setPointEnd(String pointEnd) {
        this.pointEnd = pointEnd;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getMissTime() {
        return missTime;
    }

    public void setMissTime(String missTime) {
        this.missTime = missTime;
    }

    public boolean isGoodWay() {
        return isGoodWay;
    }

    public void setGoodWay(boolean goodWay) {
        isGoodWay = goodWay;
    }

    // в зависимости от статуса завершенного маршрута испольуется разные картинки
    public int getIsGoodWay() {
        if (isGoodWay) {
            return R.drawable.goodway;
        } else {
            return R.drawable.dontgoodway;
        }
    }


//private Date date; SimpleDateFormat "hh:mm dd MMMM"

}