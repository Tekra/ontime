package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface SpinDao {

    // Добавление Person в бд
    @Insert
    void insertAll(Spin... spins);

    // Удаление Person из бд
    @Delete
    void delete(Spin spin);

    @Update
    void update(Spin spin);

    // Получение всех Person из бд
    @Query("SELECT * FROM spin")
    List<Spin> getAllSpins();

    // Получение всех Person из бд с условием

    @Insert
    void insertAll(List<Spin> allSpins);

}
