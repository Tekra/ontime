package com.example.tekra.ontime.Activitys;

import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.media.Image;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActivityChooserView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tekra.ontime.Database.HistoryWay;
import com.example.tekra.ontime.Database.HistoryWayDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;
import com.example.tekra.ontime.RecyclerView.WayAdapterHistory;
import com.example.tekra.ontime.RecyclerView.WayModelHistory;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class HistoryWayActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, WayAdapterHistory.OnItemClickListener {
    public List<HistoryWay> list;
    RecyclerView recyclerView;
    private WayAdapterHistory wayadapterHistory;
    private ArrayList<WayModelHistory> ways = new ArrayList<>();
    private HistoryWayDatabase db;
    private ImageView nullHistoryWayImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_way);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        db = Room.databaseBuilder(getApplicationContext(),
                HistoryWayDatabase.class, "database4").build();
        nullHistoryWayImage = findViewById(R.id.nullHistoryWayImage);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_history_ways);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        wayadapterHistory = new WayAdapterHistory();
        wayadapterHistory.setOnItemClickListener(this);
        recyclerView.setAdapter(wayadapterHistory);
        getListWays();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getListWays();
        if (list.size() == 0)
        {
            nullHistoryWayImage.setVisibility(View.VISIBLE);
        }
        else
        {
            nullHistoryWayImage.setVisibility(View.INVISIBLE);
        }
    }

    private void loadFromDatabase() {
        ways = getAllModels();
        wayadapterHistory.updateNotes(ways);
    }

    private ArrayList<WayModelHistory> getAllModels() {

        ArrayList<WayModelHistory> waysModel = new ArrayList<>();
        ArrayList<WayModelHistory> newWaysModel = new ArrayList<>(); //обратный ему массив


        if (list != null) {

            for (int i = 0; i < list.size(); i++) {
                // Если путь завершен, то записываю в Историю

                WayModelHistory wayModelHistory = new WayModelHistory();
                long hours = Math.round(list.get(i).wayTime / 60 / 60);
                long minute = Math.round(list.get(i).wayTime / 60);
                wayModelHistory.setDateStart(list.get(i).dayEnd +" " + getMonth(list.get(i).month));

                if(list.get(i).missTime<0)
                {wayModelHistory.setGoodWay(true);
                wayModelHistory.setMissTime("+"+(list.get(i).missTime*(-1)));}
                else
                {wayModelHistory.setGoodWay(false);
                wayModelHistory.setMissTime(String.valueOf(list.get(i).missTime));}

                wayModelHistory.setPointEnd(list.get(i).endPoint);
                wayModelHistory.setPointStart(list.get(i).startPoint);
                wayModelHistory.setTimeEnd(getZero(list.get(i).endHour)+list.get(i).endHour
                        + ":" + getZero(list.get(i).endMinute)+list.get(i).endMinute);
                waysModel.add(wayModelHistory);

            }
            for (int i = waysModel.size(); i > 0; i--) {
                newWaysModel.add(waysModel.get(i - 1));
            }

        }
        //TODO
        // Если Ways пуст, то TextView посередине "Нет истории маршрутов"
        //ПОКА ВЫВОДИТСЯ TOAST;
        else {
            nullHistoryWayImage.setVisibility(View.VISIBLE);
        }

        return newWaysModel;
    }
    private String getZero(int i) {
        if (i < 10) {
            return "0";
        } else {
            return "";
        }
    }
    // Получение и запись в list данных из БД
    private void getListWays() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                list = db.getHistoryWayDao().getAllHistoryWays();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        loadFromDatabase();
                    }
                });
            }
        });
        t1.start();
        try {
            t1.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Parse название месяца от числа
    private String getMonth(int i) {
        String month = null;
        switch (i) {
            case 1:
                return month = "Января";
            case 2:
                return month = "Февраля";
            case 3:
                return month = "Марта";
            case 4:
                return month = "Апреля";
            case 5:
                return month = "Мая";
            case 6:
                return month = "Июня";
            case 7:
                return month = "Июля";
            case 8:
                return month = "Августа";
            case 9:
                return month = "Сентября";
            case 10:
                return month = "Октября";
            case 11:
                return month = "Ноября";
            case 12:
                return month = "Декабря";
        }
        return month;
    }

    // Parse падеж минуты от числа
    private String getMinute(int i) {
        String minute = null;
        while (i > 10) {
            i = i % 10;
        }
        switch (i) {
            case 1:
                return minute = "минута";
            case 2:
                return minute = "минуты";
            case 3:
                return minute = "минуты";
            case 4:
                return minute = "минуты";
            case 5:
                return minute = "минут";
            case 6:
                return minute = "минут";
            case 7:
                return minute = "минут";
            case 8:
                return minute = "минут";
            case 9:
                return minute = "минут";
            case 0:
                return minute = "минут";
        }
        return minute;
    }

    // Скрыть шторку
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    // Установка навигации
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.show_future_ways) {
            startActivity(new Intent(this, FutureWaysActivity.class),
                    ActivityOptions.makeScaleUpAnimation(recyclerView,300,300,100,100).toBundle());

        } else if (id == R.id.create_new_way) {
            startActivity(new Intent(this, WebMapsActivity.class),
                    ActivityOptions.makeScaleUpAnimation(recyclerView,300,300,100,100).toBundle());
        } else if (id == R.id.show_history_ways) {
            startActivity(new Intent(this, HistoryWayActivity.class),
                    ActivityOptions.makeScaleUpAnimation(recyclerView,300,300,100,100).toBundle());

        }  else if (id == R.id.selected_way) {
            startActivity(new Intent(this, SelectedWayActivity.class),
                    ActivityOptions.makeScaleUpAnimation(recyclerView,300,300,100,100).toBundle());
        }
        else if (id == R.id.info)
        {
            startActivity(new Intent(this, InfoActivity.class),ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //TODO
    // По нажатию сделать диалоговое окно
    @Override
    public void onItemClick(View view, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(HistoryWayActivity.this);
        builder.setTitle("Подтверждение")
                .setMessage("Удалить маршрут из истории?")
                .setIcon(R.drawable.todosomething)
                .setCancelable(false)
                .setNeutralButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }
                ).setNegativeButton("Удалить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                Thread th = new Thread(new Runnable() {
                    public void run() {
                        db.getHistoryWayDao().delete
                                (db.getHistoryWayDao().getAllHistoryWays().
                                        get(db.getHistoryWayDao().getAllHistoryWays().size()-position-1));
                    }
                });
                th.start();
                try {
                    th.join(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                wayadapterHistory.notifyItemRemoved(position);
                new CountDownTimer(170, 170) {
                    @Override
                    public void onTick(long l) {

                    }
                    @Override
                    public void onFinish() {
                        try {
                            ways.remove(position);
                            wayadapterHistory.updateNotes(ways);
                            getListWays();
                            getAllModels();
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }.start();


                dialog.cancel();
                getListWays();
            }
        })
                .setPositiveButton("Повторить маршрут", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(getApplicationContext(),CalendarActivity.class);
                                intent.putExtra("origin",db.getHistoryWayDao().getAllHistoryWays().
                                        get(db.getHistoryWayDao().getAllHistoryWays().size()-position-1).startPoint);
                                intent.putExtra("destination",db.getHistoryWayDao().getAllHistoryWays().
                                        get(db.getHistoryWayDao().getAllHistoryWays().size()-position-1).endPoint);
                                intent.putExtra("duration",db.getHistoryWayDao().getAllHistoryWays().
                                        get(db.getHistoryWayDao().getAllHistoryWays().size()-position-1).wayTime);
                                intent.putExtra("distance",db.getHistoryWayDao().getAllHistoryWays().
                                        get(db.getHistoryWayDao().getAllHistoryWays().size()-position-1).distance);
                                startActivity(intent,ActivityOptions.makeScaleUpAnimation
                                        (recyclerView,300,300,100,100).toBundle());
                            }
                        });

                        dialogInterface.cancel();
                        thread.start();
                        try {
                            thread.join(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
