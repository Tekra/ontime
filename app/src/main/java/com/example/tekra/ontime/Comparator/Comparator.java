package com.example.tekra.ontime.Comparator;

// interface Comparator для сортировки "Ways" по дате
public interface Comparator<T> {

    long compare(T first, T second);

}
