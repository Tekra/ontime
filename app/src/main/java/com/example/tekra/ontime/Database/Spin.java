package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

// модель данных в БД
@Entity
public class Spin {
    public String textSpinner;
    public String titleSpinner;
    @PrimaryKey(autoGenerate = true)
    int Number; // номер (ID)
}