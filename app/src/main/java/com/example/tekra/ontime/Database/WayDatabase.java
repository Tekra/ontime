package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/*
 *Определение метода для получения базы данных Room
 */
@Database(entities = {Way.class}, version = 15)
public abstract class WayDatabase extends RoomDatabase {

    public abstract WayDao getWayDao();
}
