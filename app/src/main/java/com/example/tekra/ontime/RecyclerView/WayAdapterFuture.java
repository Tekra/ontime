package com.example.tekra.ontime.RecyclerView;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tekra.ontime.R;

import java.util.ArrayList;

public class WayAdapterFuture extends RecyclerView.Adapter<WayAdapterFuture.WayViewHolder> {
    protected OnItemClickListener mOnItemClickListener;
    private ArrayList<WayModelFuture> ways = new ArrayList<>();

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public WayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // описываем как создается WayViewHolder
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_future_way_item, parent, false);
        return new WayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WayViewHolder holder, int position) {
        // описываем как заполняется WayViewHolder
        WayModelFuture model = ways.get(position);
        holder.endPoint.setText(model.getEndPoint());
        holder.startPoint.setText(model.getStartPoint());
        holder.dateStart.setText(model.getDateStart());
        holder.timeStart.setText(model.getTimeStart());
        holder.timeEnd.setText(model.getTimeEnd());
        holder.dateEnd.setText(model.getDateEnd());
        holder.category.setDrawingCacheBackgroundColor(R.drawable.calendar_512x512);
    }

    @Override
    public int getItemCount() {
        return ways.size();
    }

    public void updateNotes(ArrayList<WayModelFuture> ways) {
        this.ways.clear();
        this.ways.addAll(ways);

        notifyDataSetChanged(); // обновляет все видимые элементы
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    class WayViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // Один элемент списка
        TextView startPoint;
        TextView endPoint;
        TextView timeEnd;
        TextView timeStart;
        TextView dateStart;
        TextView dateEnd;
        View category;
        View polosa; // раделяющая черта

        public WayViewHolder(View itemView) {
            super(itemView);
            // findViewById
            startPoint = itemView.findViewById(R.id.rv_point_start);
            endPoint = itemView.findViewById(R.id.rv_point_end);
            timeEnd = itemView.findViewById(R.id.rv_time_end);
            timeStart = itemView.findViewById(R.id.rv_time_start);
            dateStart = itemView.findViewById(R.id.rv_date_start);
            category = itemView.findViewById(R.id.rv_image);
            dateEnd = itemView.findViewById(R.id.rv_date_end);
            itemView.setOnClickListener(this);

        }


        @Override
        public void onClick(View view) {
            mOnItemClickListener.onItemClick(view, getAdapterPosition());
        }
    }
}
