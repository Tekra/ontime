package com.example.tekra.ontime.Activitys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.tekra.ontime.R;

public class NameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        startActivity(new Intent(getApplicationContext(),FutureWaysActivity.class));
        finish();
    }
}
