package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.util.ArrayList;

// модель данных в БД
@Entity
public class Way {
    @PrimaryKey(autoGenerate = true)
    public int Number; // номер (ID)
    public String startPoint; // начальная точка
    public String endPoint;//конечная точка
    public double distance;//дистанция маршрута
    public double wayTime;// время маршрута
    public int missTime;//время опаздания
    public int endHour;//час прибытия
    public int endMinute;//минута прибытия
    public int startHour;//чвс когда нужно начинать путь
    public int startMinute;// минута когда нужно начинать путь
    public long timeStartWay; //время пользователя когда он начал маршрут
    public long timeEndWay; //время пользователя когда он закончил маршрут
    public long DATE; // время к которому нужно прибыть (в миллисекундах)
    public int minuteToGo; // время для продварительныз сборов (для уведомлений)
    public Boolean completed;//статус завершения
    public int year;//год когда будет совершен
    public int month;//месяц когда будет совершен
    public int dayEnd;// день когда будет совершен
    public int dayStart;
    public String textSpinner;
}