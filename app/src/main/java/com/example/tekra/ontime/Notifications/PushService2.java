package com.example.tekra.ontime.Notifications;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.tekra.ontime.Activitys.FutureWaysActivity;
import com.example.tekra.ontime.Activitys.HistoryWayActivity;
import com.example.tekra.ontime.Activitys.SelectedWayActivity;
import com.example.tekra.ontime.Database.CompletedWay;
import com.example.tekra.ontime.Database.CompletedWayDatabase;
import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.Database.Way;
import com.example.tekra.ontime.Database.WayDatabase;
import com.example.tekra.ontime.R;

public class PushService2 extends IntentService {
    public static final String ON_TIME_SELECTED_WAY = "OnTimeSelectedWay";
    private CompletedWay listForAlarm;
    private CompletedWayDatabase db;
    private SpinDatabase dbSpinn;
    private Spin listOfSpin;
    private int position;
    private int number;
    private SharedPreferences settings;

    public PushService2() {
        super("");
    }

    public PushService2(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        settings = getSharedPreferences(ON_TIME_SELECTED_WAY, Context.MODE_PRIVATE);
        number = intent.getIntExtra("number", 1);
        db = Room.databaseBuilder(getApplicationContext(),
                CompletedWayDatabase.class, "database5").build(); //
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    listForAlarm = db.getCompletedWayDao().getAllCompletedWays()
                            .get(db.getCompletedWayDao().getAllCompletedWays().size() - 1);
                }
            });
            thread.start();
            try {
                thread.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            if (intent != null) {
                sendNotification(number);
            }
        }
        catch (Exception e)
        {
            Log.e("Error service", "NullSelectedWay");
        }
    }

    private void sendNotification(int i) {

//String channelId = getString(R.string.default_notification_channel_id);
        switch (i) {
            case 2000: {
                SharedPreferences.Editor editor;
                editor = settings.edit();
                editor.putBoolean("ImageSecondNotif",true);
                editor.apply();
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Intent notificationIntent = new Intent(this, SelectedWayActivity.class);
                PendingIntent contentIntent = PendingIntent.getActivity(this,
                        78, notificationIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);
                String message= "";
                if (!(listForAlarm.textSpinner.equals("") || listForAlarm.textSpinner == null))
                            message = "Не забудьте взять необходимое: " +"\n" +listForAlarm.textSpinner;
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this)
                                .setSmallIcon(R.drawable.logo)
                                .setContentTitle("Необходимо начать путь через " + listForAlarm.minuteToGo + " минут")
                                .setSound(defaultSoundUri)
                                .setVibrate(new long[]{1000, 1000, 1000,1000})
                                .setContentText("Запланированный начало пути - " + getZero(listForAlarm.startHour)+listForAlarm.startHour +
                                        ":" + getZero(listForAlarm.startMinute)+listForAlarm.startMinute)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                                .setAutoCancel(true)
                                .setContentIntent(contentIntent);

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(2,
                        notificationBuilder.build());
                stopSelf();
                return;
            }




        }
    }

    private String getZero(int i) {
        if (i < 10) {
            return "0";
        } else {
            return "";
        }
    }

    @Override
    public void onDestroy() {

        super.onDestroy();

    }
}
