package com.example.tekra.ontime.Activitys;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.tekra.ontime.Database.Spin;
import com.example.tekra.ontime.Database.SpinDatabase;
import com.example.tekra.ontime.R;

import java.util.List;

// Класс для создания нового элемента для spinner
public class CreateNewSpinnerActivity extends AppCompatActivity {
    private EditText titleSpinner;
    private EditText textSpinner;
    private SpinDatabase db;
    private List<Spin> spinList;
    private Intent intent;
    private Intent intent2;
    private int position;
    boolean editDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_spinner);
        intent = getIntent();
        String origin = intent.getStringExtra("origin");
        String destination = intent.getStringExtra("destination");
        String text = intent.getStringExtra("text");
        String title = intent.getStringExtra("title");
        position = intent.getIntExtra("pos", -1);
        double duration = intent.getDoubleExtra("duration", 10.0);
        double distance = intent.getDoubleExtra("distance", 10.0);
        final boolean edit= intent.getBooleanExtra("edit", false);
        editDelete = edit;
        String editTime = intent.getStringExtra("editTime");
        String editDate = intent.getStringExtra("editDate");
        intent2 = new Intent(getApplicationContext(), CalendarActivity.class);
        // создание ссылки на Activity для записи данных в БД

        intent2.putExtra("origin", origin);
        intent2.putExtra("destination", destination);
        intent2.putExtra("duration", duration);
        intent2.putExtra("distance", distance);
        intent2.putExtra("dateStr", editDate);
        intent2.putExtra("timeStr", editTime);
        db = Room.databaseBuilder(getApplicationContext(),
                SpinDatabase.class, "database1").build();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        titleSpinner = findViewById(R.id.edit_text_name_new_spinner);
        textSpinner = findViewById(R.id.edit_text_value_new_spinner);
        if (edit) {
            titleSpinner.setText(title);
            textSpinner.setText(text);
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Spin spin = new Spin();
                spin.titleSpinner = titleSpinner.getText().toString();
                spin.textSpinner = textSpinner.getText().toString();
                if (titleSpinner.getText().toString().equals("") || textSpinner.getText().toString().equals("")) {
                    Snackbar.make(textSpinner, "Введите значения", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else
                {
                    if (edit)
                    {
                        editSpin();

                    }
                        else
                    {
                        setSpin(spin);
                    }

                    startActivity(intent2);
                }

            }
        });


    }

    private void editSpin() {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                Spin spin = db.getSpinnerDao().getAllSpins().get(position);
                spin.titleSpinner = titleSpinner.getText().toString();
                spin.textSpinner = textSpinner.getText().toString();
                db.getSpinnerDao().update(spin);
            }
        });
        t1.start();
        try {
            t1.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void setSpin(final Spin spin) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                db.getSpinnerDao().insertAll(spin);
            }
        });
        t1.start();
        try {
            t1.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (editDelete) {
            getMenuInflater().inflate(R.menu.delete_spinner, menu);
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (editDelete) {

            //noinspection SimplifiableIfStatement
            if (id == R.id.delete) {
                delete();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void delete() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    db.getSpinnerDao().delete(db.getSpinnerDao().getAllSpins().get(position));
                }
                catch (Exception e)
                {
                    Log.e("Error","nullSpinnerList");
                }
            }
        });
        thread.start();
        try {
            thread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        startActivity(intent2);
    }
}
