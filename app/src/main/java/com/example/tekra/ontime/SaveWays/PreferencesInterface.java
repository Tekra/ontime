package com.example.tekra.ontime.SaveWays;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.tekra.ontime.Activitys.WebMapsActivity;

public class PreferencesInterface {
//TODO
// для записи объекта получаемого из карты

    public static DirectionsResult result;
    public static String origin;
    public static String destination;
    public static double duration;
    public static double distance;
    public WebMapsActivity wb = new WebMapsActivity();
    private Context context;
    private SharedPreferences prefs;

    public PreferencesInterface(Context context) {
        this.context = context;
        this.prefs = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    }

    public static DirectionsResult getResult() {
        return result;
    }

    public static void setResult(DirectionsResult result) {
        PreferencesInterface.result = result;
    }

    public static String getOrigin() {
        return origin;
    }

    public static void setOrigin(String origin) {
        PreferencesInterface.origin = origin;
    }

    public static String getDestination() {
        return destination;
    }

    public static void setDestination(String destination) {
        PreferencesInterface.destination = destination;
    }

    public static double getDuration() {
        return duration;
    }

    public static void setDuration(double duration) {
        PreferencesInterface.duration = duration;
    }

    public static double getDistance() {
        return distance;
    }

    public static void setDistance(double distance) {
        PreferencesInterface.distance = distance;
    }


/**
 * Instantiate the interface and set the context
 *//*



    public void showUncorrectedAnswer() {
                Toast.makeText(context, "Error Way", Toast.LENGTH_SHORT).show();
    }


    public void saveCallback(final String origin, final String destination, final double duration, final double distance) {
        setOrigin(origin);
        setDestination(destination);
        setDistance(distance);
        setDuration(duration);
        Toast.makeText(context, getOrigin() + " " + getDestination() + " " +
                duration + " " + distance, Toast.LENGTH_SHORT).show();
    }

*/

}
