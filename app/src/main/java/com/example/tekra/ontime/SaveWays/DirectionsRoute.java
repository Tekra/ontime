package com.example.tekra.ontime.SaveWays;

import android.graphics.Point;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

// для записи объекта получаемого из карты
class DirectionsRoute {
    public DirectionsLeg[] legs;
    public int[] waypoint_order;
    public LatLng[] overview_path;
    public Point overview_polyline;
    public LatLngBounds bounds;
    public String copyrights;
    public String[] warnings;
    public Fare fare;
}
