package com.example.tekra.ontime.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;


@Database(entities = {HistoryWay.class}, version = 4)
public abstract class HistoryWayDatabase extends RoomDatabase {
    public abstract HistoryWayDao getHistoryWayDao();
}


